import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.APK_DIR, true)

Mobile.tap(findTestObject('Adrian Ramdo Firmansyah/Mobile/Main_Page/button_login'), 0)

Mobile.tap(findTestObject('Adrian Ramdo Firmansyah/Mobile/Login_Page/Button_Register'), 0)

Mobile.setText(findTestObject('Adrian Ramdo Firmansyah/Mobile/Register_Page/Field_nama'), 'Abdul Budi', 0)

Mobile.setText(findTestObject('Adrian Ramdo Firmansyah/Mobile/Register_Page/Field_Email'), 'testcdid123@gmail.com', 0)

Mobile.setText(findTestObject('Adrian Ramdo Firmansyah/Mobile/Register_Page/Field_Whatsapp'), '089637557848', 0)

Mobile.setEncryptedText(findTestObject('Adrian Ramdo Firmansyah/Mobile/Register_Page/Field_Katasandi'), 'R2dZ4hvJ2ujoDGjQ2cClHw==', 
    0)

Mobile.setEncryptedText(findTestObject('Adrian Ramdo Firmansyah/Mobile/Register_Page/Field_KonfirmasiKatasandi'), 'R2dZ4hvJ2ujoDGjQ2cClHw==', 
    0)

Mobile.tap(findTestObject('Adrian Ramdo Firmansyah/Mobile/Register_Page/Checkbox_TNC'), 0)

Mobile.verifyElementAttributeValue(findTestObject('Adrian Ramdo Firmansyah/Mobile/Register_Page/Button_Daftar'), 'clickable', 
    'false', 0)

Mobile.closeApplication()

