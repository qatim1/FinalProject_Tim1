<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APITS_001_Cek Fungsionalitas Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>316f8005-b386-4fd3-8237-39f0941cb7d6</testSuiteGuid>
   <testCaseLink>
      <guid>5a7bd6f9-754e-4e31-abd1-2d5f06a08201</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_001_Cek Fungsionalitas Login/Negatif Test Case/APITCL_N_001_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2469db61-71d9-41fe-906f-2fdcc994ae5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_001_Cek Fungsionalitas Login/Negatif Test Case/APITCL_N_002_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bbb614f2-aba2-4065-af9c-3ea249061596</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_001_Cek Fungsionalitas Login/Negatif Test Case/APITCL_N_003_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>588a96f4-8958-4a4d-b51c-18f747e91d91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_001_Cek Fungsionalitas Login/Negatif Test Case/APITCL_N_004_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>48608a20-0eb1-4fcd-814e-4da316d95623</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_001_Cek Fungsionalitas Login/Negatif Test Case/APITCL_N_005_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7368c775-8845-4fca-b9f5-1f75fcaa7697</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_001_Cek Fungsionalitas Login/Positif Test Case/APITCL_P_001_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
