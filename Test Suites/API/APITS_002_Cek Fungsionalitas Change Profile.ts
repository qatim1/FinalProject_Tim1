<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>APITS_002_Cek Fungsionalitas Change Profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>ab2b646a-cb86-4bb4-a104-d7438e574f8a</testSuiteGuid>
   <testCaseLink>
      <guid>c47e401c-0584-4e8d-a26e-b02f5f9165d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_002_Cek Fungsionalitas Change Profile/Negatif Test Case/APITCCP_N_001_UpdateProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9f1f16f6-78a0-4020-a199-2d89b56524fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_002_Cek Fungsionalitas Change Profile/Negatif Test Case/APITCCP_N_002_UpdateProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a5a036b3-2c59-4a90-a69e-c07243c50ef2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_002_Cek Fungsionalitas Change Profile/Negatif Test Case/APITCCP_N_003_UpdateProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8b51d4af-a369-4f10-9f4b-47f36f6066e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_002_Cek Fungsionalitas Change Profile/Negatif Test Case/APITCCP_N_004_UpdateProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>30a9d5e2-6c47-4d3e-9145-f6afc359766e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_002_Cek Fungsionalitas Change Profile/Negatif Test Case/APITCCP_N_005_UpdateProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6bceed72-922b-47f0-b622-028f3e1a3491</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_002_Cek Fungsionalitas Change Profile/Positif Test Case/APITCCP_P_002_UpdateProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>93a99297-98fd-41a6-85c2-6a6f1f7b5152</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_002_Cek Fungsionalitas Change Profile/Positif Test Case/APITCCP_P_003_UpdateProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0126d338-61a6-4f04-889d-607b6a2717bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API/APITS_002_Cek Fungsionalitas Change Profile/Positif Test Case/APITCCP_P_004_UpdateProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
