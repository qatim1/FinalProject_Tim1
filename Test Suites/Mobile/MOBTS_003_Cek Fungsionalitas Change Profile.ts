<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MOBTS_003_Cek Fungsionalitas Change Profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>12b11a97-7121-4973-b58b-2ca094770789</testSuiteGuid>
   <testCaseLink>
      <guid>581d0aeb-145f-4f58-8cec-3ad64f1269a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_003_Cek Fungsionalitas Change Profile/Negatif Test Case/MOBTCCP_N_002_ChangeProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2b53431f-318d-4ac5-ba06-c0c59064fdd6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_003_Cek Fungsionalitas Change Profile/Positif Test Case/MOBTCCP_P_006_ChangeProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f4447315-00db-403b-80c6-049a7fd5ba03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_003_Cek Fungsionalitas Change Profile/Positif Test Case/MOBTCCP_P_007_ChangeProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
