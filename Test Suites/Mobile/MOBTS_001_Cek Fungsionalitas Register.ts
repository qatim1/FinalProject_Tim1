<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MOBTS_001_Cek Fungsionalitas Register</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>c8588f41-45a3-401e-9b8b-98360c5d10a5</testSuiteGuid>
   <testCaseLink>
      <guid>564151ed-e339-4fea-a219-94cc93d6c8b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_001_Cek Fungsionalitas Register/Negatif Test Case/MOBTCR_N_002_Registrasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>15bed424-0b18-4f0a-b0c0-f6399bd17542</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_001_Cek Fungsionalitas Register/Negatif Test Case/MOBTCR_N_003_Registrasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8e3af0ef-7081-45d1-9d49-a00e3e38dbf7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_001_Cek Fungsionalitas Register/Negatif Test Case/MOBTCR_N_005_Registrasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4bfa9388-734c-479c-9958-ad403c97d428</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_001_Cek Fungsionalitas Register/Negatif Test Case/MOBTCR_N_006_Registrasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5d07e224-2f09-46b1-a76f-23c2a9ad5a34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_001_Cek Fungsionalitas Register/Negatif Test Case/MOBTCR_N_008_Registrasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>92df7e0b-a96f-4598-b7b6-b13dc82d56ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_001_Cek Fungsionalitas Register/Negatif Test Case/MOBTCR_N_009_Registrasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d5c2c872-7653-46ae-b019-fb16ccf92600</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_001_Cek Fungsionalitas Register/Negatif Test Case/MOBTCR_N_011_Registrasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>57117559-b997-4c8a-ab2b-e12c08560bbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_001_Cek Fungsionalitas Register/Negatif Test Case/MOBTCR_N_014_Registrasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>643f23bd-6d6c-4cdf-89cb-a70bf9ea4619</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_001_Cek Fungsionalitas Register/Negatif Test Case/MOBTCR_N_017_Registrasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5edfea15-8458-4239-8af2-07c7f869e4bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Mobile/MOBTS_001_Cek Fungsionalitas Register/Negatif Test Case/MOBTCR_N_018_Registrasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
