<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>WEBTS_003_Cek Fungsionalitas Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>4ec7c75a-10ca-47f0-b3bc-80a6a69ad3cf</testSuiteGuid>
   <testCaseLink>
      <guid>6ecae763-8e72-4d1e-b8a9-03e0839be17a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_003_Cek Fungsionalitas Login/Negatif Test Case/WEBTCL_N_001_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7813f7a0-5b6c-418c-93d2-db36f3e6086c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_003_Cek Fungsionalitas Login/Negatif Test Case/WEBTCL_N_002_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5d27e5cc-4559-40f3-bd8a-d7c59b92d4ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_003_Cek Fungsionalitas Login/Negatif Test Case/WEBTCL_N_003_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>dde3dde9-6d95-4557-a05f-f04a389d5a0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_003_Cek Fungsionalitas Login/Negatif Test Case/WEBTCL_N_004_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ff1a33a9-bc5d-44d6-8e58-8a59956df185</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_003_Cek Fungsionalitas Login/Negatif Test Case/WEBTCL_N_005_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>49df73ad-44a9-46db-953d-0beb13d27daf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_003_Cek Fungsionalitas Login/Negatif Test Case/WEBTCL_N_006_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>186831c3-9da1-455c-8f8a-167002e4d067</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_003_Cek Fungsionalitas Login/Negatif Test Case/WEBTCL_N_007_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>cd007eff-1cb4-4c44-861a-07c75ca03414</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_003_Cek Fungsionalitas Login/Positif Test Case/WEBTCL_P_001_Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
