<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>WEBTS_005_Cek Fungsionalitas Change Profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>924e0820-09c0-47b5-ac58-7c1028b88a38</testSuiteGuid>
   <testCaseLink>
      <guid>da75e911-aeea-4bd8-ae95-53ebce3f7c54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_005_Cek Fungsionalitas Change Profile/Negatif Test Case/WEBTCCP_N_002_ChangeProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b3388c45-ee1a-498e-a0a8-526cc470926c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_005_Cek Fungsionalitas Change Profile/Negatif Test Case/WEBTCCP_N_003_ChangeProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1f76c4c3-9ac7-4b62-a854-f09c7b880677</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_005_Cek Fungsionalitas Change Profile/Negatif Test Case/WEBTCCP_N_004_ChangeProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1603b6e0-64c7-4ceb-9daf-3d43a310414e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_005_Cek Fungsionalitas Change Profile/Positif Test Case/WEBTCCP_P_006_ChangeProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d390ade3-d40d-4ced-bb70-167e4b12331d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_005_Cek Fungsionalitas Change Profile/Positif Test Case/WEBTCCP_P_007_ChangeProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>00c5fa56-31f0-45d7-b4dd-0253887eddc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Website/WEBTS_005_Cek Fungsionalitas Change Profile/Positif Test Case/WEBTCCP_P_008_ChangeProfile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
