<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>APITCCP_P_003_UpdateProfile</name>
   <tag></tag>
   <elementGuidId>37f311ba-a503-41c2-a0b0-72feb208f661</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiOTI5ODE4MDNhYzEwZWE1YmU2YTBjMTQwZTI0ZTc0N2YxMDUwYTcyZDQ2OTFjMTI3MWM3OTM4MTQwZTI5Y2NjZWQxYmVkOWZiYThhZjQ3YTkiLCJpYXQiOjE2ODcwMjU1NDYuMDM2NzQ0LCJuYmYiOjE2ODcwMjU1NDYuMDM2NzQ5LCJleHAiOjE3MTg2NDc5NDUuOTkxNDI2LCJzdWIiOiI2NCIsInNjb3BlcyI6W119.drXCTLk3OYJ7OMfETvsQAgI85wv9d3Xafk-ns3J1J5LzLATDXQTSzW4S-xLr-UX5_vQs59vho4zLLddR0uaPVyNOcnm7zVj8FdG9uF401HrTSNim5SIYyPmDWXNyBkfQmCexyhXsDy2uE68sQkTmpAxR1024V-c3OlCCKnvRNhUBvNrzPqXvJuJcO27V77BF8zH3kQFfSDBSuSr2lwcilQwMcNHNB5iv4XmzPMUHXk9tgG8gal0sI0pb99Kv0aYWe_7jeZsqwa60RZBI5diCwrIrZJ7iAKlq_wO3len1UeHngfOvxhmQrY3v-9JSpnnmXJFXdf5tQmvDQ3At3BVkBUIvvP7j1YzpZKG_tLTgHyG8iWk9iqK6SuAQmHDbPV4wP183MifouIzYScqN43NWc5vgCLx_5Ms4vPESsSRWi7HxtFojKU2rFk6zkinCYHetUGeQ8AI4MbSy0X0PysY75AnY4Dhjv5rOsE-lWf_CJOjWosjVwu9QpSAJmsxNzb_GaNZEvYLJljOnwuzhsJExwA0JP3Uh4wfxcjil-KxjJQpvKyUyxsJ_oaIhwR44DEiJdQ6sqxSOdYmvTdrDMRogAwa-igQcINex9r5zpGPcesG2fztUk4CducThhJWxHwByqnZXX9mR1jc2JB2_NJc3MB_KWnLkE_k-7IMMLcHD7D8</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;birth_date\n&quot;,
      &quot;value&quot;: &quot;1995-05-04\n\n&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
      <webElementGuid>c892717a-0bad-4b19-8258-6aa3f228fcbd</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiOTI5ODE4MDNhYzEwZWE1YmU2YTBjMTQwZTI0ZTc0N2YxMDUwYTcyZDQ2OTFjMTI3MWM3OTM4MTQwZTI5Y2NjZWQxYmVkOWZiYThhZjQ3YTkiLCJpYXQiOjE2ODcwMjU1NDYuMDM2NzQ0LCJuYmYiOjE2ODcwMjU1NDYuMDM2NzQ5LCJleHAiOjE3MTg2NDc5NDUuOTkxNDI2LCJzdWIiOiI2NCIsInNjb3BlcyI6W119.drXCTLk3OYJ7OMfETvsQAgI85wv9d3Xafk-ns3J1J5LzLATDXQTSzW4S-xLr-UX5_vQs59vho4zLLddR0uaPVyNOcnm7zVj8FdG9uF401HrTSNim5SIYyPmDWXNyBkfQmCexyhXsDy2uE68sQkTmpAxR1024V-c3OlCCKnvRNhUBvNrzPqXvJuJcO27V77BF8zH3kQFfSDBSuSr2lwcilQwMcNHNB5iv4XmzPMUHXk9tgG8gal0sI0pb99Kv0aYWe_7jeZsqwa60RZBI5diCwrIrZJ7iAKlq_wO3len1UeHngfOvxhmQrY3v-9JSpnnmXJFXdf5tQmvDQ3At3BVkBUIvvP7j1YzpZKG_tLTgHyG8iWk9iqK6SuAQmHDbPV4wP183MifouIzYScqN43NWc5vgCLx_5Ms4vPESsSRWi7HxtFojKU2rFk6zkinCYHetUGeQ8AI4MbSy0X0PysY75AnY4Dhjv5rOsE-lWf_CJOjWosjVwu9QpSAJmsxNzb_GaNZEvYLJljOnwuzhsJExwA0JP3Uh4wfxcjil-KxjJQpvKyUyxsJ_oaIhwR44DEiJdQ6sqxSOdYmvTdrDMRogAwa-igQcINex9r5zpGPcesG2fztUk4CducThhJWxHwByqnZXX9mR1jc2JB2_NJc3MB_KWnLkE_k-7IMMLcHD7D8</value>
      <webElementGuid>2768fad6-b3d5-4dae-9243-70362dc23077</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.online/api/updateprofile?birth_date=</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
