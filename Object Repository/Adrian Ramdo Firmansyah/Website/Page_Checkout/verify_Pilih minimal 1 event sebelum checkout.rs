<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_Pilih minimal 1 event sebelum checkout</name>
   <tag></tag>
   <elementGuidId>9e7ec01a-caaf-4d38-acb8-50caac6afe1c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='modalEvent']/div/div/div[2]/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-body > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>36bc862a-faae-4220-88b1-4bb497a51e1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih minimal 1 event sebelum checkout</value>
      <webElementGuid>be9b4f4f-debc-4f9b-b2ab-5f8b09b9bfe6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalEvent&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/p[1]</value>
      <webElementGuid>ebe691b1-1d20-4ce4-9fb3-e731ed1acf31</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modalEvent']/div/div/div[2]/p</value>
      <webElementGuid>164b28b0-038d-44a2-9768-fe03ea870bbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/following::p[1]</value>
      <webElementGuid>d0a814e3-d48d-4ad1-a256-7516793ce078</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm'])[1]/following::p[1]</value>
      <webElementGuid>4118fdc6-a07b-4adb-8e59-e25b3d956553</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::p[1]</value>
      <webElementGuid>830aed72-1202-4710-9de7-d053c15e27b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copyright © 2010-2021 Coding.ID All rights reserved.'])[1]/preceding::p[1]</value>
      <webElementGuid>56740760-0631-460c-8a10-139768b4f281</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pilih minimal 1 event sebelum checkout']/parent::*</value>
      <webElementGuid>223dfee4-7947-4216-a7b2-7e4f9f3f1765</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[2]/p</value>
      <webElementGuid>14ae2f77-4fa0-46fa-9435-8da34c94c0af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Pilih minimal 1 event sebelum checkout' or . = 'Pilih minimal 1 event sebelum checkout')]</value>
      <webElementGuid>40ae4d2a-24a5-4c61-9530-01d18560892c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
