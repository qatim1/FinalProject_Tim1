<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Event_Day 3</name>
   <tag></tag>
   <elementGuidId>86def00a-1426-46db-945e-12868dfbab1a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[6]/ul/li/div/div[2]/h6[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>33aa3601-cc5a-4796-a3ec-5ac85cc8dd35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_headline_6</value>
      <webElementGuid>14bf3f5d-cb9c-4215-b848-e406c97ea8f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                Day 3: Predict using Machine Learning</value>
      <webElementGuid>a916abac-531d-4eb5-8f93-9c77ee5aca0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;online-course-block&quot;]/ul[@class=&quot;list-online-course-container&quot;]/li[@class=&quot;list-online-course-item&quot;]/div[@class=&quot;cardBootcamp list-online-course-item-container&quot;]/div[@class=&quot;containerCardBootcamp&quot;]/h6[@class=&quot;new_headline_6&quot;]</value>
      <webElementGuid>e0aa83d9-9a82-445a-8089-d834c0294ea6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[6]/ul/li/div/div[2]/h6[2]</value>
      <webElementGuid>b5dc918a-5af6-4489-83cc-5b27222061d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EVENT'])[1]/following::h6[1]</value>
      <webElementGuid>69915890-3dd4-49cb-bbb8-a5b955a20e35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::h6[2]</value>
      <webElementGuid>de7411e4-01a3-4181-9dd1-29d88ce3f45c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ziyad Syauqi Fawwazi'])[2]/preceding::h6[1]</value>
      <webElementGuid>45e5554c-1f18-46d3-82e3-15586079d275</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[2]/preceding::h6[1]</value>
      <webElementGuid>5b182750-1d85-4db0-94c2-b716be633646</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Day 3: Predict using Machine Learning']/parent::*</value>
      <webElementGuid>861d2f2d-4edd-44ee-847b-7669f48ac4aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/h6[2]</value>
      <webElementGuid>a7929b1e-a5e5-42a0-93d5-4f96d311e599</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = '
                                Day 3: Predict using Machine Learning' or . = '
                                Day 3: Predict using Machine Learning')]</value>
      <webElementGuid>72b315d2-d170-499b-9d75-fade4c4e2a9d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
