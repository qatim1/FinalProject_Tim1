<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icon_user profile</name>
   <tag></tag>
   <elementGuidId>c00c692a-d587-4429-a103-022502151641</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[7]/a/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.fas.fa-user-alt</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>ea076d09-22db-4f27-b86a-27c240ffd654</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fas fa-user-alt</value>
      <webElementGuid>8059f23e-1e24-469e-ba36-e9795ee9364d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;___class_+?26___&quot;]/a[1]/i[@class=&quot;fas fa-user-alt&quot;]</value>
      <webElementGuid>ac5fc269-49f9-4c2f-ae88-fdde171b4e74</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[7]/a/i</value>
      <webElementGuid>636932da-09b2-4fdf-a900-e76beb7a00d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/i</value>
      <webElementGuid>2bad8259-1aa4-4ca0-afe0-99363b791609</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
