<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>APITCCP_N_004_UpdateProfile</name>
   <tag></tag>
   <elementGuidId>e137e314-a462-4952-aac2-2ed09e5f1b2d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZTg2MjhkYmQyOTI0MTliNjk3ZmRiOWUxMzA4OGRjYzMzZDViZTY1NjQ5NjM3ODhjMjMwMTkxOGMyNzhjNGRjOTRjYzQ2NTcyNjY2NDA0ZTAiLCJpYXQiOjE2ODcxMzEyMzAuNTQ1MzA5LCJuYmYiOjE2ODcxMzEyMzAuNTQ1MzExLCJleHAiOjE3MTg3NTM2MzAuNTQyNzU0LCJzdWIiOiI2NCIsInNjb3BlcyI6W119.hmZs6fZmgManydmYKYsk26X40XzYczy6mXGUlhB5cnFHC4Qq_JY5I463QcIPnFW8AOf3NlHqorcn9k1HtwBpOcKWRxjzkW9HU5FzDSj-0RDPZKzRXAEn1NLWv1V0Uhvg3cuVqLiqNx_ztLeO2_8mQeLJO8wU2Y7plu2hezLWWGOLL87rYzbfVCmump7SJq25R3jVJFBsxYqqBIBlXr9WFqhUwLa1l-mPUaaBeJXRSLefoIWfjtVC2BCwUXPLmhhdNYaSQg4BxgfQ_Juzd_iQ5YjtcD7twY_O64Oz3wkV_LkBDSNDjE59abhUklocdVhAhnx_vwkKnp-RuayLpmlhSB_fv5uURzaZzsjzJs3O67jQZK__5tukoGUr-OOf3dVF4VfcV8NQ7WeLJbsvgG1oB1IaVrf9KCLSNWvvdg_LWvGG0EvlK3VhyCflziLsAn3CfD7eq17Pe-vnPVQpbuvCZEoMRYEsX4yVFVDK5jg0gL1afZoPF208cN-gjwd09r0KFspET2lxWliup2Pidq_T8v6vX_pe8S3fLHWIE6n63nfRZEXQ5yvjgKDpLBBqk2MChyLqTAlM3neGdC7dxM31qoemId2amxH6B58buwbwF7PTHrOZE-LmC316AhwJWevCllfD4jt_Xc8G9B2cG4yhJ6BcMn6luLPA6PIuNkFVgFc</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;Pictures/txt file.txt&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>c7a73e2c-e374-4583-99a4-1264883c231f</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZTg2MjhkYmQyOTI0MTliNjk3ZmRiOWUxMzA4OGRjYzMzZDViZTY1NjQ5NjM3ODhjMjMwMTkxOGMyNzhjNGRjOTRjYzQ2NTcyNjY2NDA0ZTAiLCJpYXQiOjE2ODcxMzEyMzAuNTQ1MzA5LCJuYmYiOjE2ODcxMzEyMzAuNTQ1MzExLCJleHAiOjE3MTg3NTM2MzAuNTQyNzU0LCJzdWIiOiI2NCIsInNjb3BlcyI6W119.hmZs6fZmgManydmYKYsk26X40XzYczy6mXGUlhB5cnFHC4Qq_JY5I463QcIPnFW8AOf3NlHqorcn9k1HtwBpOcKWRxjzkW9HU5FzDSj-0RDPZKzRXAEn1NLWv1V0Uhvg3cuVqLiqNx_ztLeO2_8mQeLJO8wU2Y7plu2hezLWWGOLL87rYzbfVCmump7SJq25R3jVJFBsxYqqBIBlXr9WFqhUwLa1l-mPUaaBeJXRSLefoIWfjtVC2BCwUXPLmhhdNYaSQg4BxgfQ_Juzd_iQ5YjtcD7twY_O64Oz3wkV_LkBDSNDjE59abhUklocdVhAhnx_vwkKnp-RuayLpmlhSB_fv5uURzaZzsjzJs3O67jQZK__5tukoGUr-OOf3dVF4VfcV8NQ7WeLJbsvgG1oB1IaVrf9KCLSNWvvdg_LWvGG0EvlK3VhyCflziLsAn3CfD7eq17Pe-vnPVQpbuvCZEoMRYEsX4yVFVDK5jg0gL1afZoPF208cN-gjwd09r0KFspET2lxWliup2Pidq_T8v6vX_pe8S3fLHWIE6n63nfRZEXQ5yvjgKDpLBBqk2MChyLqTAlM3neGdC7dxM31qoemId2amxH6B58buwbwF7PTHrOZE-LmC316AhwJWevCllfD4jt_Xc8G9B2cG4yhJ6BcMn6luLPA6PIuNkFVgFc</value>
      <webElementGuid>ca8b026d-fea0-4d36-b496-b1aa0973abc5</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.0</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.online/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
