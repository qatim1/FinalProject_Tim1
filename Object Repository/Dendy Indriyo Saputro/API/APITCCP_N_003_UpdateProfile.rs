<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>Update Foto Profile menggunakan .pdf</description>
   <name>APITCCP_N_003_UpdateProfile</name>
   <tag></tag>
   <elementGuidId>1073f805-fba5-4443-803d-ee1612bd4c02</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiYWVlZjNhYzA1OTdjMjI1ZTE2Y2VhNzA3NWNjNjQwNjczM2M0N2RmMGFmNThiYjc2ZTMxYWU1ZjhlOGQ5YjMxNzljMTE4YWE0MjA2Mzg5NmQiLCJpYXQiOjE2ODcxMjgzNTEuMDE3MTgyLCJuYmYiOjE2ODcxMjgzNTEuMDE3MTg0LCJleHAiOjE3MTg3NTA3NTEuMDE0Mzk5LCJzdWIiOiI2NCIsInNjb3BlcyI6W119.MarPbHJfMQdF7GztmWDj8tsgAZJI3XnP5x8-IFMVPlx6EVMAcgSO91NGVKrdZGMJmZVGf6gwNah9VTUmBraOm3_9RtVVedXPGFggpVogzHnnNI3TRG-5CSaa48RcMJyfjgicL5amPsJiSOdah52b6UMddrOmigJjUgrRBwKuQ5C5ohQayqu1LQIuD6nJA4IhV94dfi5GnJAH3lt6f5_gbRYgNLvY2y6-4fOuo_eWM4ayHL5yuTT-Err75r-J8d5Ptr7W19Z7k_iYLn-901pMOnnDCSVHyQSmkmmKwtbDVSncIsKJc6k7HKY09dGY2P4dhOOrAPqwociOfbPLGDn70lZsMVsTv3utDhNzDo2d2hsQ_Nahd0UutX8kD4svimw7h5BBXTqmhnQNJonlDclaPOuRVYIfXaykwkm4vvLM5exp87dsYLntpgnspiIZ_I2fW5sXonXhbop8zS4Bi_LC1lS79SoV0Yz1-NFqP9wR_lZzBKbe8YpweVRkvmWpmkoGo4uNpBhzRmrflMHwz7bbslrdyHJAwNKERxmENSQ6KzOt3mfZ4DtYKhW7WERaRsrix6nByR2zbPopD0q8cqAThF3bUYXRwAB8f0x9cB6MZhlqlHu27lrs8zn5iNCuIFS2Tj9oIKC1Mg57vVWxcmL8NkNTclbuepUPJUiGkzprGoQ</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;Pictures/pdf file.pdf&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>90384558-fd48-4471-ab93-d1c76826e71b</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiYWVlZjNhYzA1OTdjMjI1ZTE2Y2VhNzA3NWNjNjQwNjczM2M0N2RmMGFmNThiYjc2ZTMxYWU1ZjhlOGQ5YjMxNzljMTE4YWE0MjA2Mzg5NmQiLCJpYXQiOjE2ODcxMjgzNTEuMDE3MTgyLCJuYmYiOjE2ODcxMjgzNTEuMDE3MTg0LCJleHAiOjE3MTg3NTA3NTEuMDE0Mzk5LCJzdWIiOiI2NCIsInNjb3BlcyI6W119.MarPbHJfMQdF7GztmWDj8tsgAZJI3XnP5x8-IFMVPlx6EVMAcgSO91NGVKrdZGMJmZVGf6gwNah9VTUmBraOm3_9RtVVedXPGFggpVogzHnnNI3TRG-5CSaa48RcMJyfjgicL5amPsJiSOdah52b6UMddrOmigJjUgrRBwKuQ5C5ohQayqu1LQIuD6nJA4IhV94dfi5GnJAH3lt6f5_gbRYgNLvY2y6-4fOuo_eWM4ayHL5yuTT-Err75r-J8d5Ptr7W19Z7k_iYLn-901pMOnnDCSVHyQSmkmmKwtbDVSncIsKJc6k7HKY09dGY2P4dhOOrAPqwociOfbPLGDn70lZsMVsTv3utDhNzDo2d2hsQ_Nahd0UutX8kD4svimw7h5BBXTqmhnQNJonlDclaPOuRVYIfXaykwkm4vvLM5exp87dsYLntpgnspiIZ_I2fW5sXonXhbop8zS4Bi_LC1lS79SoV0Yz1-NFqP9wR_lZzBKbe8YpweVRkvmWpmkoGo4uNpBhzRmrflMHwz7bbslrdyHJAwNKERxmENSQ6KzOt3mfZ4DtYKhW7WERaRsrix6nByR2zbPopD0q8cqAThF3bUYXRwAB8f0x9cB6MZhlqlHu27lrs8zn5iNCuIFS2Tj9oIKC1Mg57vVWxcmL8NkNTclbuepUPJUiGkzprGoQ</value>
      <webElementGuid>1c46afa3-3a82-4050-bde1-79ef34a38a12</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.0</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.online/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
